<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'scotchbox' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '$V.IWG,zExIkBNIbJ2qW0gQly0V*,1Xc% jHPA54OGxZ]wL=OC^c>d5BtKYo+iW;' );
define( 'SECURE_AUTH_KEY',   '(VOPtjU-aHl?^jvFEza`f}Dm)@Yb:{97N^#3pST32&H;f+Ns#&&2V27jZaWlCq.&' );
define( 'LOGGED_IN_KEY',     '}N82/gK:hUj5%y}Ir31Sd3u8M:gcAk*ot3QU=V?yef92eZc?g> hu,h}{o%rw,ty' );
define( 'NONCE_KEY',         '1KP+y9$a3[IqK>6!g4SDtkG&s)j#!UQcq/qT%lqA(t%tM._0.d/P_Q|O4$L|-Q83' );
define( 'AUTH_SALT',         '`,=bX#cd)V`m]$h)o_vY3U$/)tF]]3LzLh)$qAn#ihac*QF5Gx~Y@q-#>.6sZb:F' );
define( 'SECURE_AUTH_SALT',  'juRuT69}2]VG=~ohRI_DLW[&j+ <)lPf^:Y$uPr?Z!oJUM7;DD4jZr;:7!SD5*xn' );
define( 'LOGGED_IN_SALT',    'E^HoFL.@-.o9}w-m8*3 }*w35LQx|nFAd:4D4C7dO4%wwfL3jj]Z+?e8n`)sM3+B' );
define( 'NONCE_SALT',        'AFYgz{AUE8b-q._Iz_FQZ:^eV{IS>Vi2OA=91d@<|)92@Ps)yIk_mOgc3Fo(-A`k' );
define( 'WP_CACHE_KEY_SALT', ']3dKD<xbruVbjsv{}0q82h9Hah(=[SM?mF>wjt|,t9[Y_~a}AY#3HHlO5&@}pzw0' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * S3 Configuration
 */

define('AS3CF_SETTINGS', serialize(array(
    'provider' => 'aws',
    'access-key-id' => 'AKIAJ7CMJDXRM34BF5FQ',
    'secret-access-key' => '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',
)));

if (file_exists(dirname( __FILE__ ) . '/' . 'wp-config.local.php')) require_once(dirname( __FILE__ ) . '/' . 'wp-config.local.php');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
