<?php function thb_post( $atts, $content = null ) {
	$style = '';
	$atts = vc_map_get_attributes( 'thb_post', $atts );
	extract( $atts );

 	$posts = vc_build_loop_query($source);
 	$posts = $posts[1];

 	$classes[] = 'posts-shortcode align-stretch';
 	$classes[] = $style;
 	$classes[] = in_array($style, array('style1','style2','style3','style4','style5','style6-alt','style8')) ? 'row' : false;
 	$classes[] = $style === 'style5' ? 'masonry' : false;
 	ob_start();
 	?>
 	<div class="<?php echo esc_attr(implode(' ', $classes)); ?>">
		<?php $i = 0; if ($posts->have_posts()) :  while ($posts->have_posts()) : $posts->the_post(); ?>
			<?php
			if ($style !== 'style8') {
				set_query_var( 'columns', $columns );
				set_query_var( 'style2_size', '' );
				get_template_part( 'inc/templates/postbit/'.$style);
				set_query_var( 'style2-full', false );
			} else if ($style == 'style8') {
				set_query_var( 'columns', $style8_columns );
				if ($i == 0) {
					get_template_part( 'inc/templates/postbit/style8-first');
				} else {
					get_template_part( 'inc/templates/postbit/style8');
				}
			}
			?>
		<?php $i++; endwhile; else : endif; ?>
	</div>
	<?php
	$out = ob_get_clean();

	wp_reset_query();
	wp_reset_postdata();

	return $out;
}
thb_add_short('thb_post', 'thb_post');