<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $css
 * @var $el_id
 * @var $equal_height
 * @var $content_placement
 * @var $content - shortcode content
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Row_Inner
 */
$el_class = $equal_height = $content_placement = $css = $el_id = '';
$disable_element = '';
$output = $after_output =  $thb_video_bg = $bg_video_overlay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$el_class = $this->getExtraClass( $el_class );
$thb_column_padding = $thb_column_padding ? 'no-column-padding': '';
$css_classes = array(
	'vc_row',
	'wpb_row', //deprecated
	'vc_inner',
	'vc_row-fluid',
	$thb_max_width,
	$thb_column_padding,
	$el_class,
	vc_shortcode_custom_css_class( $css ),
);
if ( 'yes' === $disable_element ) {
	if ( vc_is_page_editable() ) {
		$css_classes[] = 'vc_hidden-lg vc_hidden-xs vc_hidden-sm vc_hidden-md';
	} else {
		return '';
	}
}

if (vc_shortcode_custom_css_has_property( $css, array('border', 'background') )) {
	$css_classes[]='vc_row-has-fill';
}

if (!empty($atts['gap'])) {
	$css_classes[] = 'vc_column-gap-'.$atts['gap'];
}

if ( ! empty( $equal_height ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-equal-height';
}

if ( ! empty( $content_placement ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-content-' . $content_placement;
}

if ( ! empty( $flex_row ) ) {
	$css_classes[] = 'vc_row-flex';
}

$wrapper_attributes = array();
// build attributes for wrapper
if ( ! empty( $el_id ) ) {
	$wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
}

/* Video BG */
if ( $thb_video_bg !== '' ) {
	$bg_type = '';
	$video_type = wp_check_filetype( $thb_video_bg, wp_get_mime_types() );
	$pattern = '/background-image:\s*url\(\s*([\'"]*)(?P<file>[^\1]+)\1\s*\)/i';
	preg_match($pattern, $css, $bg_image);

	$wrapper_attributes[] = 'data-vide-bg="'.$video_type['ext'].': '.esc_attr($thb_video_bg). ( isset($bg_image[2]) ? ', poster: '.esc_attr($bg_image[2]) : '').'"';
		
	if (isset($bg_image[2])) {
		$bg_url = strtok($bg_image[2], '?');
		$bg_type = wp_check_filetype( $bg_url, wp_get_mime_types() );
	}
	
	$wrapper_attributes[] = 'data-vide-options="posterType: '.( isset($bg_image[2]) ? esc_attr($bg_type['ext']) : 'none').', loop: true, muted: true, position: 50% 50%, resizing: true"';
	if ( $thb_video_overlay_color != '' ) {
		$bg_video_overlay = '<div class="thb_video_overlay" style="background-color: '.esc_attr($thb_video_overlay_color) .';"></div>';
	}
	
	$css_classes[] = 'thb_video_bg';
}

$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( array_unique( $css_classes ) ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

$output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
$output .= wpb_js_remove_wpautop( $content );
$output .= $bg_video_overlay;
$output .= '</div>';
$output .= $after_output;

echo $output;