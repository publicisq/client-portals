<?php
  add_filter('excerpt_length', 'thb_supershort_excerpt_length');

	$vars = $wp_query->query_vars;
	$thb_masonry = array_key_exists('thb_masonry', $vars) ? $vars['thb_masonry'] : false;
	$thb_size = array_key_exists('thb_size', $vars) ? $vars['thb_size'] : false;
	$thb_style = array_key_exists('thb_style', $vars) ? $vars['thb_style'] : false;
	$thb_animation = array_key_exists('thb_animation', $vars) ? $vars['thb_animation'] : false;
	$thb_i = array_key_exists('thb_i', $vars) ? $vars['thb_i'] : false;
	$id = get_the_ID();
	$image_id = get_post_thumbnail_id($id);
	$image_url = wp_get_attachment_image_src($image_id, 'full');
	$aspect_ratio = $image_id ? (($image_url[2] / $image_url[1]) * 100).'%' : '100%';
	$aspect_ratio = $thb_masonry ? $aspect_ratio : '80%';
	$hover_id = get_post_meta($id, 'main_hover_image', true);

	$main_color_title = get_post_meta($id, 'main_color_title', true);

  if ('portfolio' == get_post_type($id)) {
		$categories = get_the_term_list( $id, 'portfolio-category', '', ', ', '' );
		if ($categories !== '' && !empty($categories)) {
			$categories = strip_tags($categories);
		}

		$terms = get_the_terms( $id, 'portfolio-category' );
	} else {
		$categories = get_the_term_list( $id, 'category', '', ', ', '' );
		if ($categories !== '' && !empty($categories)) {
			$categories = strip_tags($categories);
		}

		$terms = get_the_terms( $id, 'category' );
	}

	$cats = '';
	if (!empty($terms)) {
		foreach ($terms as $term) { $cats .= ' thb-cat-'.strtolower($term->slug); }
	} else {
		$cats = '';
	}

	$class[] = 'medium-6 columns';
	$class[] = 'type-portfolio';
	$class[] = $thb_size;
	$class[] = $thb_animation;
	$class[] = 'light-title';
	$class[] = $thb_i % 2 == 0 ? '' : 'style3_even';
	$class[] = $thb_style;
	$class[] = $cats;
	$class[] = 'carousel_style3';
	$class[] = 'portfolio-'.get_the_ID();

	$main_listing_type = get_post_meta($id, 'main_listing_type', true);
	$permalink = '';
	if ($main_listing_type == 'lightbox') {
		$permalink = $image_url[0];
		$class[] = 'portfolio-image-links';
		$class[] = 'mfp-image';
	} else if ($main_listing_type == 'link') {
		$permalink = get_post_meta($id, 'main_listing_link', true);
	} else {
		$permalink = get_the_permalink();
	}
?>
<a href="<?php echo esc_url($permalink); ?>" title="<?php the_title_attribute(); ?>" <?php post_class($class); ?> id="portfolio-<?php the_ID(); ?>">
	<div class="portfolio-holder">
		<div class="carousel_style3_content">
  		<aside class="thb-categories"><?php echo esc_html($categories); ?></aside>
  		<h3><?php the_title(); ?></h3>
  		<div class="post-excerpt">
  			<?php echo get_the_excerpt(); ?>
  		</div>
		</div>
		<div class="portfolio-inner <?php echo esc_attr($thb_hover_style); ?>">
			<?php the_post_thumbnail('werkstatt-rectangle-3x'); ?>
		</div>
	</div>
</a>