<?php
	$format = get_post_format();
	$permalink = get_the_permalink();
	if ($format === 'link') {
		$permalink = get_post_meta(get_the_ID(), 'post_link', true);
	}
?>
<div class="row max_width">
	<div class="small-12 columns">
		<article itemscope itemtype="http://schema.org/Article" <?php post_class('post style9'); ?> role="article">
			<aside class="post-meta">
				<?php echo get_the_date(); ?>
			</aside>
			<header class="post-title entry-header">
				<h5 class="entry-title" itemprop="name headline">
					<?php the_title(); ?>
					<figure class="post-gallery medium-shadow"><?php the_post_thumbnail('werkstatt-squarelarge'); ?></figure>
				</h5>
			</header>
			<?php do_action( 'thb_PostMeta' ); ?>
			<a href="<?php echo esc_url($permalink); ?>" class="style9-post-link"></a>
		</article>
	</div>
</div>