<?php

function thb_get_hero_templates($template_list) {
	$template_list['hero_section_01'] = array(
		'name' => esc_html__( 'Hero Section - 01', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e1.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true"][vc_column][thb_portfolio_slider button_style="thb-3d-style" thb_subtitles="subtitles" thb_header_colors="thb_change_header" autoplay="1" portfolio_ids="284,62,60,274"][/vc_column][/vc_row]',
	);

	$template_list['hero_section_02'] = array(
		'name' => esc_html__( 'Hero Section - 02', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e2.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" thb_color="light-title" full_height="yes" content_placement="middle" thb_scroll_bottom="true" thb_scroll_bottom_color="light" css=".vc_custom_1502869087384{background: #000000 url(https://werkstatt.fuelthemes.net/werkstatt-designer/wp-content/uploads/sites/9/2016/09/h_p_01.jpg?id=312) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="align-center header-row"][vc_column thb_color="thb-light-column" offset="vc_col-lg-9 vc_col-md-11" el_class="text-center"][vc_column_text animation="animation fade-in"]
		<h1><span style="color: #ffd909;">Hello, I’m Thomas.</span>
		Now, as the business of standing mast-heads.</h1>
		[/vc_column_text][/vc_column][/vc_row]',
	);

	$template_list['hero_section_03'] = array(
		'name' => esc_html__( 'Hero Section - 03', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e3.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" full_height="yes" content_placement="middle" thb_divider_position="bottom" css=".vc_custom_1488275770751{padding-top: 10vh !important;padding-bottom: 10vh !important;background-image: url(http://werkstatt.fuelthemes.net/werkstatt-freelancer/wp-content/uploads/sites/8/2016/09/fhsa.jpg?id=314) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column thb_color="thb-light-column" el_class="text-center"][thb_image retina="retina_size" animation="animation fade-in" alignment="center" image="315"][vc_empty_space height="35px"][thb_fadetype fade_text="#E-8_JTNDaDUlMjBzdHlsZSUzRCUyMmZvbnQtc2l6ZSUzQSUyMDIwcHglM0IlMjIlM0UlMkFJbmRlcGVuZGVudCUyMENyZWF0aXZlJTIwRGlyZWN0b3IlMjAlMjYlMjBQaG90b2dyYXBoZXIlMkElM0MlMkZoNSUzRSUwQQ=="][/vc_column][/vc_row]',
	);

	$template_list['hero_section_04'] = array(
		'name' => esc_html__( 'Hero Section - 04', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e4.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" full_height="yes" content_placement="middle" thb_divider_position="bottom" css=".vc_custom_1475676332986{padding-top: 15vh !important;padding-bottom: 15vh !important;background-image: url(http://werkstatt.fuelthemes.net/werkstatt-personal/wp-content/uploads/sites/7/2016/09/h16_s_01.jpg?id=285) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][thb_image retina="retina_size" animation="animation bottom-to-top" alignment="center" image="286"][/vc_column][/vc_row]',
	);

	$template_list['hero_section_05'] = array(
		'name' => esc_html__( 'Hero Section - 05', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e5.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" full_height="yes" content_placement="middle" thb_scroll_bottom="true" el_class="align-center" css=".vc_custom_1488655986911{padding-top: 10vh !important;padding-bottom: 10vh !important;background-image: url(http://werkstatt.fuelthemes.net/werkstatt-agency-modern/wp-content/uploads/sites/12/2016/09/w_170.jpg?id=367) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column el_class="text-center" offset="vc_col-lg-6 vc_col-md-8"][thb_image retina="retina_size" animation="animation fade-in" alignment="center" image="323"][vc_empty_space][vc_column_text css=".vc_custom_1476438856385{padding-right: 5% !important;padding-left: 5% !important;}"]
		<h5 style="font-size: 20px;"><strong>Most times, ideacide happens without us even realizing it. A possible off-the-wall idea or solution appears like a blip.</strong></h5>
		[/vc_column_text][/vc_column][/vc_row]',
	);

	$template_list['hero_section_06'] = array(
		'name' => esc_html__( 'Hero Section - 06', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e6.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" full_height="yes" content_placement="middle" thb_scroll_bottom="true" thb_scroll_bottom_style="style2" thb_scroll_bottom_color="light" thb_divider_position="bottom" thb_video_bg="http://werkstatt.fuelthemes.net/werkstatt-agency-digital/wp-content/uploads/sites/19/2016/11/agency-digital-video-bg.mp4" css=".vc_custom_1517340253029{padding-top: 10vh !important;padding-bottom: 10vh !important;}" el_class="mast-head"][vc_column][thb_image retina="retina_size" animation="animation fade-in" alignment="center" image="353"][/vc_column][/vc_row]',
	);

	$template_list['hero_section_07'] = array(
		'name' => esc_html__( 'Hero Section - 07', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e7.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" css=".vc_custom_1480240880366{margin-bottom: 100px !important;}"][vc_column][thb_portfolio_slider slider_style="slider_style2" button_style="thb-3d-style" thb_subtitles="subtitles" thb_header_colors="thb_change_header" portfolio_ids="381,382,383"][/vc_column][/vc_row]',
	);

	$template_list['hero_section_08'] = array(
		'name' => esc_html__( 'Hero Section - 08', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e8.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_divider_position="bottom" el_id="hello" css=".vc_custom_1530037156932{padding-top: 10vh !important;padding-bottom: 10vh !important;background-color: #101010 !important;}"][vc_column thb_color="thb-light-column" width="1/4"][vc_column_text]Hello Everyone[/vc_column_text][vc_empty_space height="30px"][/vc_column][vc_column thb_color="thb-light-column" width="3/4"][vc_column_text]</p>
		<h1><strong>There is Napoleon; who, upon the top of the column of <span style="text-decoration: line-through; color: #848484;">vendome</span>, <span style="text-decoration: line-through; color: #848484;">stands with</span>arms <span style="text-decoration: line-through; color: #848484;">folded</span> some one hundred and fifty feet in the air.</strong></h1>
		<p><span style="color: #cccccc;">When the entire ships company were assembled, and with curious and not wholly unapprehensive faces, were eyeing him, for he looked not unlike the weather horizon when a storm is coming up.</span></p>
		<p><span style="color: #ffffff;"><a class="behance" style="color: #ffffff;" href="https://themeforest.net/item/werkstatt-creative-portfolio-theme/17870799?ref=fuelthemes">Behance</a> — <a class="twitter" style="color: #ffffff;" href="https://themeforest.net/item/werkstatt-creative-portfolio-theme/17870799?ref=fuelthemes">Twitter</a> — <a class="fivehundred" style="color: #ffffff;" href="https://themeforest.net/item/werkstatt-creative-portfolio-theme/17870799?ref=fuelthemes" target="_blank" rel="noopener">500px</a>  — <a class="linkedin" style="color: #ffffff;" href="https://themeforest.net/item/werkstatt-creative-portfolio-theme/17870799?ref=fuelthemes" target="_blank" rel="noopener">linkedin</a></span>[/vc_column_text][/vc_column][/vc_row]',
	);

	$template_list['hero_section_09'] = array(
		'name' => esc_html__( 'Hero Section - 09', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e9.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_color="light-title" thb_scroll_bottom="true" thb_scroll_bottom_style="style3" thb_scroll_bottom_color="light" thb_divider_position="bottom" thb_video_overlay_color="rgba(0,0,0,0.4)" el_class="align-center" thb_row_title="Intro" css=".vc_custom_1517346652481{background-image: url(http://werkstatt.fuelthemes.net/werkstatt-landing-tasty/wp-content/uploads/sites/27/2016/09/video.jpg?id=397) !important;}" thb_video_bg="http://werkstatt.fuelthemes.net/werkstatt-landing-tasty/wp-content/uploads/sites/27/2017/08/tasty.mp4"][vc_column thb_color="thb-light-column" width="5/12"][vc_column_text animation="animation bottom-to-top"]
		<h1><strong>Tasty <span style="color: #d5bd90;"><em>Recipes</em></span> with Video Instructions</strong></h1>
		[/vc_column_text][vc_column_text animation="animation bottom-to-top"]After years of dreaming, planning, and hard work, we’ve officially launched The Oh She Glows Recipe App for iOS and Android devices! Our recipe app features the most popular.[/vc_column_text][thb_button style="thb-pill-style" color="white" animation="animation bottom-to-top" link="url:%23features|title:Learn%20More||"][/vc_column][vc_column width="5/12" css=".vc_custom_1500149187900{padding-top: 40% !important;padding-right: 10% !important;padding-left: 10% !important;}"][thb_image full_width="true" animation="animation bottom-to-top" image="389"][/vc_column][/vc_row]',
	);

	$template_list['hero_section_10'] = array(
		'name' => esc_html__( 'Hero Section - 10', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e10.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" thb_color="light-title" thb_scroll_bottom="true" thb_scroll_bottom_style="style3" thb_scroll_bottom_color="light" css=".vc_custom_1500632548414{background: #1484f4 url(http://werkstatt.fuelthemes.net/werkstatt-landing-sphere/wp-content/uploads/sites/28/2016/09/slider_bg.jpg?id=388) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][vc_row_inner thb_max_width="max_width" content_placement="middle"][vc_column_inner thb_color="thb-light-column" width="1/2" css=".vc_custom_1500491232965{padding-top: 60px !important;padding-right: 22% !important;padding-bottom: 60px !important;padding-left: 15% !important;}"][vc_column_text animation="animation bottom-to-top"]
		<h1 class="h1"><strong>Tell the unique story of your life</strong></h1>
		[/vc_column_text][vc_column_text animation="animation bottom-to-top"]Now, as the business of standing mast-heads, ashore or afloat, is a very ancient and interesting one.[/vc_column_text][thb_button style="thb-pill-style" color="white" animation="animation bottom-to-top" link="url:https%3A%2F%2Fthemeforest.net%2Fitem%2Fwerkstatt-creative-portfolio-theme%2F17870799%3Fref%3Dfuelthemes|title:Get%20Started||"][/vc_column_inner][vc_column_inner el_class="text-center" width="1/2" css=".vc_custom_1500487698366{padding-top: 20% !important;padding-right: 30% !important;padding-left: 15% !important;}"][thb_image retina="retina_size" animation="animation bottom-to-top" alignment="right" image="389"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
	);

	$template_list['hero_section_11'] = array(
		'name' => esc_html__( 'Hero Section - 11', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e11.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_shape_divider="true" thb_divider_position="bottom" css=".vc_custom_1513006525183{padding-top: 25vh !important;padding-bottom: 40vh !important;background-color: #5619b3 !important;}" divider_shape="waves_opacity" thb_divider_height="200" thb_divider_color="#ffffff"][vc_column][vc_row_inner thb_max_width="max_width" el_class="align-center"][vc_column_inner thb_color="thb-light-column" el_class="text-center" offset="vc_col-lg-6 vc_col-md-8 vc_col-xs-12"][thb_fadetype fade_text="
		<h1>*Featured Work*</h1>
		"][thb_fadetype fade_text="*Check out my work below and let me know if you are interested*

		"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
	);

	$template_list['hero_section_12'] = array(
		'name' => esc_html__( 'Hero Section - 12', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e12.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_divider_position="bottom" el_class="align-center" css=".vc_custom_1475072484539{margin-top: 100px !important;padding-top: 10vh !important;padding-bottom: 10vh !important;}"][vc_column offset="vc_col-lg-7 vc_col-md-9" el_class="text-center"][vc_column_text]
		<h6>WELCOME TO WERKSTATT</h6>
		[/vc_column_text][thb_autotype typed_text="
		<h1><strong>We are a full fledged digital
		agency creating *artworks;concepts;interiors;dreams*</strong></h1>
		" loop="1"][/vc_column][/vc_row][vc_row thb_divider_position="bottom"][vc_column][thb_portfolio_grid columns="large-6" title_position="title-topleft" portfolio_ids="224,226,228,230,232,234"][/vc_column][/vc_row]',
	);

	$template_list['hero_section_13'] = array(
		'name' => esc_html__( 'Hero Section - 13', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e13.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row css=".vc_custom_1494440531129{padding-bottom: 10vh !important;}"][vc_column offset="vc_col-lg-9 vc_col-md-8"][vc_column_text animation="animation fade-in"]
		<h1 style="font-weight: 500; line-height: 1.1;">Hello, I’m Peter Moldrich.
		Art Director living in London.</h1>
		The rigging lived. The mast-heads, like the tops of tall palms, were outspreadingly tufted with arms and legs. Clinging to a spar with one hand.[/vc_column_text][thb_button animation="animation fade-in" link="url:http%3A%2F%2Fwerkstatt.fuelthemes.net%2Fwerkstatt-lateral%2Fabout-us-agency%2F|title:About%20Me||"][/vc_column][/vc_row][vc_row]',
	);

	$template_list['hero_section_14'] = array(
		'name' => esc_html__( 'Hero Section - 14', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/hero/hero-e14.jpg",
		'cat' => array( 'hero' ),
		'sc' => '[vc_row thb_full_width="true" content_placement="bottom" thb_scroll_bottom="true" thb_scroll_bottom_style="style3" thb_divider_position="bottom" css=".vc_custom_1541492053563{margin-bottom: 10vh !important;padding-top: 25px !important;padding-bottom: 25px !important;}"][vc_column width="1/4" el_class="text-center"][/vc_column][vc_column width="2/4" css=".vc_custom_1541443981678{padding-top: 20vh !important;padding-bottom: 20vh !important;}" el_class="text-center"][thb_fadetype fade_text="
<h1><strong>*Hello. We re
Rudiger*</strong></h1>
" style="style2" extra_class="hero-text"][/vc_column][vc_column width="1/4" el_class="text-right"][thb_video_lightbox style="lightbox-style3" icon_size="inline" icon_pulse="thb-icon-pulse" animation="animation fade-in" video="https://vimeo.com/70585618" video_text="WATCH VIDEO" icon_color="#2f43ff"][/vc_column][/vc_row]',
	);

	return $template_list;
}