<?php 

function thb_get_testimonials_templates($template_list) {
  $template_list['testimonials_section_01'] = array(
  	'name' => esc_html__( 'Testimonials Section - 01', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/testimonials/testimonials-e1.jpg",
  	'cat' => array( 'testimonials', 'about' ),
  	'sc' => '[vc_row thb_full_width="true" thb_divider_position="bottom" css=".vc_custom_1475009886874{padding-top: 10vh !important;padding-bottom: 10vh !important;background-color: #f9f9f9 !important;}" el_class="align-center"][vc_column offset="vc_col-lg-8 vc_col-md-9"][vc_column_text animation="animation bottom-to-top"]
  	<h6 style="text-align: center;">Testimonials</h6>
  	[/vc_column_text][thb_testimonial_parent][thb_testimonial quote="“The Tharks were having their hands full in the center of the room, and I began to realize that nothing short of a miracle could save Dejah.”" author_name="Isaac Tobin" author_title="Creative Director at Fantasy Interactive" author_image="118"][thb_testimonial quote="“Or kind rest bred with am shed then. In raptures building an bringing be. Elderly is detract tedious assured private so to visited. Do travelling companions contrasted it.”" author_name="Jason Bourne" author_title="Technical Director at Fantasy Interactive" author_image="122"][thb_testimonial quote="“Started several mistake joy say painful removed reached end. State burst think end are its.”" author_name="John Carpenter" author_title="Financial Director at Fantasy Interactive" author_image="123"][/thb_testimonial_parent][/vc_column][/vc_row]',
  );
  $template_list['testimonials_section_02'] = array(
  	'name' => esc_html__( 'Testimonials Section - 02', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/testimonials/testimonials-e2.jpg",
  	'cat' => array( 'testimonials', 'about' ),
  	'sc' => '[vc_row el_id="testimonials" css=".vc_custom_1500455307868{padding-top: 10vh !important;padding-bottom: 10vh !important;}"][vc_column][vc_row_inner thb_max_width="max_width" el_class="align-center"][vc_column_inner offset="vc_col-lg-8 vc_col-md-10"][vc_column_text animation="animation bottom-to-top" el_class="text-center" css=".vc_custom_1500629901071{padding-right: 15% !important;padding-left: 15% !important;}"]
  	<h2><em><strong>WHAT PEOPLE ARE SAYING</strong></em></h2>
  	[/vc_column_text][vc_column_text animation="animation bottom-to-top" el_class="text-center" css=".vc_custom_1500629909022{padding-right: 15% !important;padding-left: 15% !important;}"]Accordingly I searched for a hiding place and finally found one by accident, inside a huge hanging ornament which swung.[/vc_column_text][vc_empty_space height="30px"][/vc_column_inner][/vc_row_inner][thb_testimonial_parent thb_style="style3" columns="large-4"][thb_testimonial quote="``Very handy to show new doctors what meds I’m currently taking, plus frequency and dosages! Without having to carry all my pill bottles with me...``" author_name="Fred Wilson" author_title="Design Director at Apple" author_image="205"][thb_testimonial quote="``Mango has been a big help in keeping me on track with my medications. The additional info on drug interactions has also been very informative. “" author_name="Andrew Mason" author_title="Product Designer at Apple" author_image="210"][thb_testimonial quote="``Overall, great theme with lots of features!
  	Hope, some will be added with next updates, like - dropcaps, maybe dynamic gradient backgrounds. But still, great job guys!``" author_name="Betty Bailey" author_title="Designer at Microsoft" author_image="208"][thb_testimonial quote="``Very handy to show new doctors what meds I’m currently taking, plus frequency and dosages! Without having to carry all my pill bottles with me...``" author_name="Jason Bruth" author_title="Developer at Google" author_image="207"][/thb_testimonial_parent][/vc_column][/vc_row]',
  );
  return $template_list;
}
