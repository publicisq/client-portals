<?php

function thb_get_portfolio_listing_templates($template_list) {

  $template_list['portfolio_listing_01'] = array(
  	'name' => __( 'Portfolio-Listing - Style 1', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e1.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true"][vc_column][thb_portfolio_masonry add_filters="true" filter_categories="44,41,43,42" portfolio_ids="39,41,43,45,47,49,52,55,57,62,60,64"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_02'] = array(
  	'name' => __( 'Portfolio-Listing - Style 2', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e2.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" css=".vc_custom_1475745150566{padding-top: 100px !important;padding-bottom: 10vh !important;}"][vc_column][thb_portfolio_masonry style="style2" columns="thb-5" add_filters="true" filter_categories="44,41,42,45" portfolio_ids="39,41,43,45,226,55,52,49,57,62,60,64"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_03'] = array(
  	'name' => __( 'Portfolio-Listing - Style 3', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e3.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_divider_position="bottom"][vc_column][thb_portfolio_grid columns="large-6" title_position="title-topleft" portfolio_ids="224,226,228,230,232,234"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_04'] = array(
  	'name' => __( 'Portfolio-Listing - Style 4', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e4.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row css=".vc_custom_1475596167512{padding-bottom: 4vh !important;}" el_class="align-center"][vc_column][thb_portfolio_masonry style="style2" columns="large-4" portfolio_ids="274,313,317,319,323,321,325,327,329"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_05'] = array(
  	'name' => __( 'Portfolio-Listing - Style 5', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e5.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true"][vc_column][thb_portfolio_grid columns="large-3" title_position="title-bottomleft" portfolio_ids="52,39,41,43,45,47,49,55,57,60,62,64"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_06'] = array(
  	'name' => __( 'Portfolio-Listing - Style 6', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e6.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true"][vc_column][thb_portfolio_masonry masonry_layout="masonry-style4" add_filters="true" filter_categories="44,43,42,45" portfolio_ids="39,41,43,45,47,49,52,55,57"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_07'] = array(
  	'name' => __( 'Portfolio-Listing - Style 7', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e7.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" thb_color="light-title" full_height="yes" el_class="align-center" css=".vc_custom_1502869106552{background-color: #000000 !important;}"][vc_column thb_color="thb-light-column"][thb_portfolio_list thb_style="thb-light" portfolio_ids="39,41,43,45,47,49"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_08'] = array(
  	'name' => __( 'Portfolio-Listing - Style 8', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e8.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_divider_position="bottom" el_class="align-center" css=".vc_custom_1475743219594{padding-top: 10vh !important;padding-bottom: 10vh !important;}"][vc_column thb_color="thb-light-column" offset="vc_col-lg-9 vc_col-md-11"][thb_portfolio_grid thb_margins="margins" columns="large-6" hover_style="thb-border-hover" portfolio_ids="39,41,45,43,47,49"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_09'] = array(
  	'name' => __( 'Portfolio-Listing - Style 9', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e9.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" el_class="text-center" css=".vc_custom_1476283631798{padding-bottom: 2vh !important;}"][vc_column][vc_column_text animation="animation fade-in"]<span style="font-size: 12px; color: #111; letter-spacing: 0.2em;">FEATURED WORKS</span>[/vc_column_text][vc_empty_space height="40px"][thb_portfolio_masonry masonry_layout="masonry-style5" thb_margins="margins" hover_style="thb-push-bottom" portfolio_ids="43,47,52"][vc_empty_space height="40px"][thb_button style="thb-text-style" animation="animation fade-in" icon="" link="url:http%3A%2F%2Fwerkstatt.fuelthemes.net%2Fwerkstatt-agency-modern%2Fprojects%2F|title:VIEW%20ALL%20WORKS||"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_10'] = array(
  	'name' => __( 'Portfolio-Listing - Style 10', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e10.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" css=".vc_custom_1479061421076{padding-top: 50px !important;padding-bottom: 10vh !important;}"][vc_column][thb_portfolio_masonry masonry_layout="masonry-style6" thb_margins="margins" title_position="title-bottomleft" hover_style="thb-gradient-hover" animation_style="thb-scale" add_filters="true" filter_categories="70,74,72,73,71" portfolio_ids="39,41,43,45,226,55,52,49,57,62,60,64,224,47,230,228,232,234,236,270,274,313,317"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_11'] = array(
  	'name' => __( 'Portfolio-Listing - Style 11', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e11.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" thb_divider_position="bottom" css=".vc_custom_1479405433490{padding-top: 12vh !important;}"][vc_column][vc_column_text animation="animation bottom-to-top" css=".vc_custom_1479403854278{padding-right: 10% !important;padding-left: 10% !important;}"]
  	<h6 style="text-align: center;"><span style="color: #4e59ff;">WHAT WE DO</span></h6>
  	[/vc_column_text][vc_column_text animation="animation fade-in" css=".vc_custom_1479411334741{padding-right: 10% !important;padding-left: 10% !important;}"]
  	<p class="h1" style="text-align: center;"><span style="color: #292c2e;">Lets Make Something Beautiful</span></p>
  	[/vc_column_text][vc_empty_space height="80px"][thb_portfolio_grid columns="large-4" title_position="title-bottomleft" hover_style="thb-default-small" portfolio_ids="39,41,43,45,47,49"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_12'] = array(
  	'name' => __( 'Portfolio-Listing - Style 12', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e12.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row el_class="align-center"][vc_column offset="vc_col-lg-8 vc_col-md-10"][thb_portfolio_grid style="style2" thb_margins="margins" columns="large-4" add_filters="true" filter_style="style2" filter_categories="70,72,73" portfolio_ids="375,376,377,378,379,380"][vc_row_inner][vc_column_inner el_class="text-center"][vc_empty_space height="20px"][thb_button style="thb-solid-border" link="url:http%3A%2F%2Fwerkstatt.fuelthemes.net%2Fwerkstatt-architecture%2Fprojects%2F|title:View%20All%20Projects||"][vc_empty_space height="100px"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_13'] = array(
  	'name' => __( 'Portfolio-Listing - Style 13', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e13.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_divider_position="bottom" el_id="works" css=".vc_custom_1530039721202{padding-top: 15vh !important;padding-bottom: 15vh !important;background-color: #101010 !important;}"][vc_column thb_color="thb-light-column" width="1/4"][vc_column_text]Selected Projects[/vc_column_text][vc_empty_space height="30px"][/vc_column][vc_column thb_color="thb-light-column" width="3/4"][thb_portfolio_text portfolio_source="advanced" add_filters="true" filter_style="style3" filter_categories="44,41,43,40" source="size:16|order_by:date|post_type:portfolio"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_14'] = array(
  	'name' => __( 'Portfolio-Listing - Style 14', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e14.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row][vc_column][thb_portfolio_masonry masonry_layout="masonry-style7" thb_margins="margins" portfolio_source="advanced" title_position="title-topleft" hover_style="thb-corner-hover" add_filters="true" filter_style="style3" filter_categories="44,41,43,45" loadmore="true" source="size:9|post_type:portfolio"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_15'] = array(
  	'name' => __( 'Portfolio-Listing - Style 15', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e15.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true"][vc_column][thb_portfolio_bg_grid columns="large-3" title_position="title-topleft" portfolio_ids="226,306,224,230,228,60,232,57,234,64"][/vc_column][/vc_row]',
  );

  $template_list['portfolio_listing_16'] = array(
  	'name' => __( 'Portfolio-Listing - Style 16', 'werkstatt' ),
  	'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/listing/listing-e16.jpg",
  	'cat' => array( 'Portfolio-Listing' ),
  	'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" thb_divider_position="bottom"][vc_column][thb_portfolio_masonry style="style1" masonry_layout="custom" portfolio_source="advanced" hover_style="thb-corner-arrow" animation_speed="0.7" source="size:4|post_type:portfolio"][/vc_column][/vc_row]',
  );

  return $template_list;
}
