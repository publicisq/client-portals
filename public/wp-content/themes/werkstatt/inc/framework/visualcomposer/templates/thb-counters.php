<?php 

function thb_get_counters_templates($template_list) {
	$template_list['counters_section_01'] = array(
		'name' => esc_html__( 'Counters Section - 01', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/counters/counter-e1.jpg",
		'cat' => array( 'counters' ),
		'sc' => '[vc_row thb_divider_position="bottom" css=".vc_custom_1479236980021{padding-top: 15vh !important;padding-bottom: 15vh !important;}" el_class="align-center"][vc_column offset="vc_col-lg-10 vc_col-md-11" css=".vc_custom_1479408518508{padding-right: 2% !important;padding-left: 2% !important;}" el_class="xlarge-9"][vc_row_inner][vc_column_inner width="1/4"][thb_counter icon="software_transform_bezier.svg" speed="1000" counter="41064" heading="Pixels Pushed"][/vc_column_inner][vc_column_inner width="1/4"][thb_counter icon="basic_spread_text.svg" speed="1500" counter="380" heading="Articles Devoured"][/vc_column_inner][vc_column_inner width="1/4"][thb_counter icon="basic_picture.svg" counter="830" heading="Photos Used"][/vc_column_inner][vc_column_inner width="1/4"][thb_counter icon="basic_headset.svg" speed="2500" counter="2362" heading="Hours Listened"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
	);
	
	return $template_list;
}