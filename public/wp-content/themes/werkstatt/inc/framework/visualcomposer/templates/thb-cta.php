<?php 

function thb_get_cta_templates($template_list) {
	$template_list['cta_section_01'] = array(
		'name' => esc_html__( 'CTA Section - 01', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/cta/cta-e1.jpg",
		'cat' => array( 'cta' ),
		'sc' => '[vc_row thb_full_width="true" css=".vc_custom_1474984884861{padding-top: 20vh !important;padding-bottom: 15vh !important;background-image: url(http://werkstatt.fuelthemes.net/werkstatt-agency/wp-content/uploads/sites/2/2016/09/a_01_opt.jpg?id=292) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="align-center"][vc_column offset="vc_col-lg-8 vc_col-md-9" el_class="text-center"][vc_column_text animation="animation fade-in" css=".vc_custom_1474984222233{padding-right: 15% !important;padding-left: 15% !important;}"]
		<h1><span style="color: #ef173b;">Let’s Work Together</span></h1>
		At last, passage paid, and luggage safe, we stood on board the schooner. Hoisting sail, it glided down the Acushnet river. On one side, New Bedford rose in terraces of streets, their ice-covered trees all glittering in the clear, cold air. Huge hills and mountains of casks on casks were piled.[/vc_column_text][thb_button style="thb-border-style" animation="animation bottom-to-top" icon="" link="url:https%3A%2F%2Fthemeforest.net%2Fitem%2Fwerkstatt-creative-portfolio-theme%2F17870799%3Fref%3Dfuelthemes|title:Get%20Started|target:%20_blank|"][/vc_column][/vc_row]',
	);
	$template_list['cta_section_02'] = array(
		'name' => esc_html__( 'CTA Section - 02', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/cta/cta-e2.jpg",
		'cat' => array( 'cta' ),
		'sc' => '[vc_row thb_full_width="true" parallax="content-moving" parallax_image="362" thb_divider_position="bottom" css=".vc_custom_1488552457183{padding-top: 15vh !important;padding-bottom: 15vh !important;background-color: #191818 !important;background-position: center;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="align-center"][vc_column thb_color="thb-light-column" el_class="text-center brush-column" offset="vc_col-lg-6 vc_col-md-10" css=".vc_custom_1479406153102{background-image: url(http://werkstatt.fuelthemes.net/werkstatt-agency-digital/wp-content/uploads/sites/19/2016/09/f_02.png?id=349) !important;}"][vc_column_text]
		<h6 style="text-align: center;">ESTABLISHED 2014</h6>
		[/vc_column_text][vc_column_text animation="animation fade-in"]
		<p class="h1" style="text-align: center;">A Creative &amp; Production
		Studio, that loves to make
		you look good.</p>
		[/vc_column_text][thb_button style="thb-text-style" color="white" link="url:https%3A%2F%2Fthemeforest.net%2Fitem%2Fwerkstatt-creative-portfolio-theme%2F17870799|title:Lets%20Work%20Together|target:%20_blank|"][/vc_column][/vc_row]',
	);
	$template_list['cta_section_03'] = array(
		'name' => esc_html__( 'CTA Section - 03', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/cta/cta-e3.jpg",
		'cat' => array( 'cta' ),
		'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" thb_color="light-title" css=".vc_custom_1500628989345{padding-top: 13vh !important;padding-bottom: 13vh !important;background: #1478f7 url(http://werkstatt.fuelthemes.net/werkstatt-landing-sphere/wp-content/uploads/sites/28/2016/09/gradient.png?id=431) !important;}" el_id="download"][vc_column thb_color="thb-light-column" el_class="text-center"][vc_column_text animation="animation bottom-to-top" el_class="text-center"]
		<h1 class="h1"><strong>Let the music flow!</strong></h1>
		[/vc_column_text][thb_button style="thb-pill-style" color="white" animation="animation bottom-to-top" link="url:https%3A%2F%2Fthemeforest.net%2Fitem%2Fwerkstatt-creative-portfolio-theme%2F17870799%3Fref%3Dfuelthemes|title:Download%20Now|target:%20_blank|"][/vc_column][/vc_row]',
	);
	$template_list['cta_section_04'] = array(
		'name' => esc_html__( 'CTA Section - 04', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/cta/cta-e4.jpg",
		'cat' => array( 'cta' ),
		'sc' => '[vc_row el_class="align-center center-quote" css=".vc_custom_1476206960211{margin-top: 10vh !important;margin-bottom: 10vh !important;padding-top: 10vh !important;padding-bottom: 10vh !important;background-image: url(http://werkstatt.fuelthemes.net/werkstatt-agency-modern/wp-content/uploads/sites/12/2016/09/m_a_01.jpg?id=321) !important;}"][vc_column el_class="text-center" offset="vc_col-lg-7 vc_col-md-9"][vc_column_text animation="animation fade-in"]<span style="font-size: 12px; color: #111; letter-spacing: 0.2em;">WHO WE ARE</span>[/vc_column_text][vc_column_text animation="animation fade-in"]
		<h1>We’re crazy about innovations and improving life with technology We pour our skills and passion into creating a digital product.</h1>
		[/vc_column_text][thb_button style="thb-text-style" animation="animation fade-in" icon="" link="url:https%3A%2F%2Fthemeforest.net%2Fitem%2Fwerkstatt-creative-portfolio-theme%2F17870799%3Fref%3Dfuelthemes|title:LEARN%20MORE|target:%20_blank|"][/vc_column][/vc_row]',
	);
	
	return $template_list;
}