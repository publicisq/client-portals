<?php

function thb_get_blog_templates($template_list) {
	$template_list['blog_section_01'] = array(
		'name' => esc_html__( 'Blog Section - 01', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/blog/blog-e1.jpg",
		'cat' => array( 'blog', 'about' ),
		'sc' => '[vc_row css=".vc_custom_1476283779757{padding-bottom: 15vh !important;}" el_class="align-center"][vc_column offset="vc_col-lg-10 vc_col-md-10" css=".vc_custom_1476283406219{padding-top: 10vh !important;}"][vc_column_text animation="animation fade-in" el_class="text-center"]<span style="font-size: 12px; color: #111; letter-spacing: 0.2em;">LATEST AND GREATEST POSTS</span>[/vc_column_text][vc_empty_space height="20px"][thb_post style="style6-alt" columns="large-4" item_count="3"][vc_empty_space height="20px"][vc_row_inner][vc_column_inner el_class="text-center"][thb_button style="thb-text-style" animation="animation fade-in" icon="" link="url:http%3A%2F%2Fwerkstatt.fuelthemes.net%2Fwerkstatt-agency-modern%2Fblog%2F|title:VIEW%20ALL%20POSTS||"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
	);
  $template_list['blog_section_02'] = array(
		'name' => esc_html__( 'Blog Section - 02', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/blog/blog-e2.jpg",
		'cat' => array( 'blog', 'about' ),
		'sc' => '[vc_row thb_divider_position="bottom" css=".vc_custom_1541581151469{padding-top: 14vh !important;padding-bottom: 16vh !important;}"][vc_column][thb_slidetype slide_text="
<h3>*Latest Posts*</h3>
" style="style2" thb_animated_color="#6dfd8c"][vc_empty_space][vc_row_inner][vc_column_inner width="2/3"][thb_slidetype slide_text="
<h2>*This is selection of blogs talking about;design industry and agency.*</h2>
" thb_animated_color="#0a0a0a"][/vc_column_inner][vc_column_inner el_class="text-right" width="1/3"][thb_button style="thb-border-style" animation="animation fade-in" link="url:http%3A%2F%2Fwerkstatt.fuelthemes.net%2Fwerkstatt-rudiger%2Fblog%2F|title:View%20All%20Posts||"][/vc_column_inner][/vc_row_inner][vc_empty_space height="40px"][thb_post style="style9" source="size:6|post_type:post"][/vc_column][/vc_row]',
	);
	return $template_list;
}