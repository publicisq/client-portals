<?php

function thb_get_about_templates($template_list) {
	$template_list['about_section_01'] = array(
		'name' => esc_html__( 'About Section - 01', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/about/about-e1.jpg",
		'cat' => array( 'about' ),
		'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" thb_divider_position="bottom" css=".vc_custom_1541579311012{padding-top: 16vh !important;padding-bottom: 16vh !important;background-color: #fbfbdb !important;}"][vc_column][vc_row_inner thb_max_width="max_width"][vc_column_inner width="1/2" css=".vc_custom_1541580477962{padding-top: 10vh !important;}"][thb_slidetype slide_text="
<h2>*We design new business;models, create new services,;<em>products</em> and <em>experiences</em>,;and work with clients.*</h2>
" thb_animated_color="#1f2a69"][vc_column_text animation="animation fade-in" css=".vc_custom_1541579912642{padding-right: 10% !important;}"]<span style="color: #1f2a69;">Creation and rejuvenation of brand thinking. We develop positioning strategies which allow brands to transcend the confines of their category and occupy deeply meaningful territories. We also take a brand-led view of innovation.</span>[/vc_column_text][thb_button_text style="style4" animation="animation right-to-left" link="url:https%3A%2F%2Fthemeforest.net%2Fitem%2Fwerkstatt-creative-portfolio-theme%2F17870799%3Fref%3Dfuelthemes|title:Our%20Studio|target:%20_blank|"][/vc_column_inner][vc_column_inner width="1/2"][thb_image animation="animation right-to-left" image="394"][/thb_image][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
	);
  $template_list['about_section_02'] = array(
		'name' => esc_html__( 'About Section - 02', 'werkstatt' ),
		'thumbnail' => Thb_Theme_Admin::$thb_theme_directory_uri."assets/img/admin/about/about-e2.jpg",
		'cat' => array( 'about' ),
		'sc' => '[vc_row thb_full_width="true" thb_row_padding="true" thb_column_padding="true" thb_divider_position="bottom" css=".vc_custom_1541581338894{padding-top: 12vh !important;padding-bottom: 12vh !important;background: #2546d5 url(http://werkstatt.fuelthemes.net/werkstatt-rudiger/wp-content/uploads/sites/34/2018/11/r06.jpg?id=396) !important;}"][vc_column][vc_row_inner thb_max_width="max_width"][vc_column_inner][thb_slidetype slide_text="
<h3>*Get in Touch*</h3>
" style="style2" thb_animated_color="#ffffff"][/vc_column_inner][/vc_row_inner][vc_row_inner thb_max_width="max_width"][vc_column_inner width="2/3"][thb_slidetype slide_text="
<h2>*Have any questions? Feel free
to say hello - hi@fuelthemes.net *</h2>
" thb_animated_color="#ff6f6f"][/vc_column_inner][vc_column_inner el_class="text-right" width="1/3"][/vc_column_inner][/vc_row_inner][vc_row_inner thb_max_width="max_width" css=".vc_custom_1541582281807{padding-top: 4vh !important;}"][vc_column_inner width="1/4"][thb_image retina="retina_size" animation="animation top-to-bottom" image="397"][/thb_image][/vc_column_inner][vc_column_inner width="1/4"][thb_image retina="retina_size" animation="animation top-to-bottom" image="398"][/thb_image][/vc_column_inner][vc_column_inner width="1/4"][thb_image retina="retina_size" animation="animation top-to-bottom" image="399"][/thb_image][/vc_column_inner][vc_column_inner width="1/4"][thb_image retina="retina_size" animation="animation top-to-bottom" image="400"][/thb_image][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner el_class="text-center"][vc_empty_space height="46px"][thb_button style="thb-border-style" color="white" animation="animation fade-in" link="url:https%3A%2F%2Finstagram.com%2Ffuelthemes%2F|title:Follow%20Us%20on%20Instagram|target:%20_blank|"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
	);
	return $template_list;
}